// API - Application Programming Interface

// [SECTION] JavaScript Synchronous vs Asynchronous
// JavaScript by default is synchronous meaning that only one statement is executed at a time.


//This can be proven when a statement has an error, JavaScript will not proceed with the next statement
console.log('Hello World');
// conosle.log('Hello Again'); //Syntax Error
console.log('Goodbye');

/*
for(let i = 0; i <= 1500; i++) {
	console.log(i);
}
*/

// this code will not execute while the loop code above is not yet done
// console.log("Hello Again")

// Asynchronous
// - means that we can proceed to execute other statements, while time consuming code/s is running in the background


// the fetch API allows you to asynchronously request for a resource data
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

// Syntax: fetch('URL', options) 
//https://developer.mozilla.org/en-US/docs/Web/API/fetch

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// the "then" method captures the "response" object and returns another promise which will eventually be resolved or rejected
fetch('https://jsonplaceholder.typicode.com/posts/')
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/')

// use the "json" method from the "response" object and returns another "promise"
.then(response => response.json())

// print converted JSON value from the "fetch request"
.then(json => console.log(json));

// the "async" and "await" keywords is another approach that can be used to achieve asynchronous code

async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');
	console.log(result);

	let json = await result.json();
	console.log(json);

	/*
	let specificDocument = await fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response)=>response.json())
	.then((json) => console.log(json));

	let putNewDocument = await fetch('https://jsonplaceholder.typicode.com/posts/1', 
	{
		method: 'PUT',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'New car',
			body: 'Just got my new red tesla',
			userId: 1,
		})	
	})
	.then((response)=>response.json())
	.then((json)=> console.log(json));
	*/

}

fetchData()

// [SECTION] Getting a specific document
//https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())
.then(json => console.log(json))

// Retrieving titles of the whole collection
/*
fetch('https://jsonplaceholder.typicode.com/posts')// fetch the whole collection
.then(response => response.json()) // it converts out json object response to js object
.then(json => {json.forEach(post => console.log(post.title))})
*/

// Retrieving a title of a specific document
fetch('https://jsonplaceholder.typicode.com/posts/87')// fetch the whole collection
.then(response => response.json()) // it converts out json object response to js object
.then(json => console.log(`The retrieved title "${json.title}"`))



// [SECTION] Creating a specific post

fetch('https://jsonplaceholder.typicode.com/posts/', 
{
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'OOTD',
		body: 'New outfit',
		userId: 1
	})
})					//converts json object to js object
.then(response=>response.json())
.then(json=>console.log(json));

// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1',
{	
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'New car',
		body: 'Just got my new red tesla',
		userId: 1
	})
})

.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post using PATCH method 
// PUT vs PATCH
	// PATCH is used to update a single/several properties
	// PUT is used to update the whole document/object
fetch('https://jsonplaceholder.typicode.com/posts/1', 
{
	method: "PATCH",
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})
})

.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', 
{
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json));

// GET, POST, PUT, PATCH, DELETE